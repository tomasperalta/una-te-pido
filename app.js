// Imports
const config = require('./config')
const NewsAPI = require("newsapi")
const Twit = require('twit')
const sentiment = require("sentiment-multilang")

const news = new NewsAPI(config.news_api_key) // News API object

setInterval(() => {
  news.v2.topHeadlines({
    country: 'ar',
    language: 'es',
    q: 'coronavirus',
    pageSize: 100
  }).then(response => {
    const articles = response.articles

    articles.forEach(article => {
      const title = article.title
      const isPositive = sentiment(title, 'es').vote === 'positive'

      console.log("El titulo: " + title + ". Es: " + sentiment(title, 'es').vote)
      console.log("---------------")

      if (isPositive) {
        const url = article.url
        const toTwit = title + " " + url // The string we'll tweet

        // Twit object
        const twitter = new Twit(config.twit)

        // Tweet the positive news! 🎉
        twitter.post('statuses/update', { status: toTwit }, (err, data, response) => {
          console.log(response.statusCode)
        })
      }
    })
  })
}, 1000 * 60)